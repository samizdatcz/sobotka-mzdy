---
title: "Na Sobotkovu daňovou reformu by doplatila pětina Pražanů. Nejvíc by si polepšilo Karlovarsko a Pardubicko"
perex: "Premiér Sobotka na začátku týdne oznámil, že chystá progresivní zdanění zaměstnanců, na kterém by 90 procent z nich vydělalo. Spočítali jsme distribuci platů a mezd v každém kraji – a tedy i to, kde a kdo by nejvíc získal nebo tratil."
description: "Premiér Sobotka na začátku týdne oznámil, že chystá progresivní zdanění zaměstnanců, na kterém by 90 procent z nich vydělalo. Spočítali jsme distribuci platů a mezd v každém kraji – a tedy i to, kde a kdo by nejvíc získal nebo tratil."
authors: ["Jan Boček"]
published: "23. února 2017"
coverimg: https://samizdat.cz/data/sobotka-mzdy/www/media/cover.jpg
coverimg_note: "Tisková konference ministerstva vnitra, Bohuslav Sobotka. Foto: Martin Svozílek, ČRo"
socialimg: https://samizdat.cz/data/sobotka-mzdy/www/media/socialimg.jpg
url: "sobotka-mzdy"
libraries: [jquery, highcharts]
recommended:
  - link: https://interaktivni.rozhlas.cz/krajske-volby-v-okrscich/
    title: Jak volili vaši sousedi?
    perex: Prohlédněte si nejpodrobnější mapu volebních výsledků
    image: https://interaktivni.rozhlas.cz/media/51445ce4a3c687cbb2d35b797e18e359/600x_.jpg
  - link: https://interaktivni.rozhlas.cz/duvera-politikum/
    title: Analýza ČRo: Sobotka u voličů ČSSD neztrácí důvěru
    perex: Říjnová data z průzkumu veřejného mínění ukazují, že kauzy posledních měsíců Sobotkovi neubližují.
    image: https://interaktivni.rozhlas.cz/media/a5c1edc60116e7c2760e255090258265/600x_.jpg
  - link: http://www.rozhlas.cz/zpravy/technika/_zprava/stoleti-brouka-hitlerovo-lidove-auto-prezilo-valku-a-svet-si-ho-zamiloval--1584151
    title: ČSSD ztrácela napříč republikou, ANO triumfuje
    perex: Mapa zisků v letošních krajských volbách ukazuje, že voliči odcházejí od sociálních demokratů k ANO 2011.
    image: https://interaktivni.rozhlas.cz/media/c4d8ceec27f29211c75af1aab6ae5cc6/600x_.jpg
---

Přibližně pětina pražských zaměstnanců loni vydělávala víc než 48 955 korun hrubého. Právě to je částka, kdy se daňová reforma, navržená premiérem Sobotkou, přestává vyplácet. Kdo vydělává méně, měl by si podle připravovaných pravidel o několik stovek polepšit. Kdo víc, toho by čekala změna k horšímu.

<aside class="big">
  <div id="zlepseni" style="height:600px;">
</aside>

Právě v Praze je podíl těch, které by nová pravidla nepotěšila, nejvyšší. Hrubou mzdu nad zmíněnou částku loni mělo 19,4 procent zaměstnanců v soukromém i státním sektoru; nad 100 tisíc korun hrubého braly 4,2 procenta zaměstnanců. Nejvyšší medián příjmů mají podle dat ministerstva práce a sociálních věcí řídící pracovníci v oblasti reklamy a styku s veřejností (99,5 tisíce korun), nejnižší číšníci a servírky (11,2 tisíce korun).

<div data-bso="1"></div>

Naopak nejvíc těch, kdo by si polepšili, byl loni v Karlovarském a Pardubickém kraji. Sobotkova reforma by zde polepšila 95,3 procentům, respektive 94,3 procentům zaměstnanců.

Spočítali jsme to analýzou dat ministerstva práce a sociálních věcí. To pro všechny kraje každoročně zveřejňuje podrobnou [statistiku příjmů](https://portal.mpsv.cz/sz/stat/vydelky) v soukromém i státním sektoru. Z ní jsme pro každý kraj vymodelovali pravděpodobné rozložení příjmů. Následující graf ukazuje právě rozložení příjmů pro jednotlivé kraje.

<aside class="big">
  <div id="prijmy" style="height:600px;">
</aside>

Celkově by si skutečně polepšilo slibovaných 90 procent zaměstnanců. Jak ovšem ukazuje následující graf, nevydělali by na reformě mnoho: ti s příjmy do 30 tisíc hrubého by si polepšili o čtyři procenta současného příjmu, ti s 30 až 49 tisíci hrubého ještě méně. Naopak u lépe vydělávajících by byl propad oproti současnému stavu výrazný, při hrubém příjmu 100 tisíc korun by čistý příjem klesl o 11,5 procenta.

<aside class="big">
  <div id="zmena" style="height:600px;">
</aside>